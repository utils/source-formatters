#!/bin/sh
# This formatter uses the `pep8` tool to perform format checks of files written
# in the Python language.
#
# The `pep8` tool does *not* perform the formatting, but only checks the
# formatting.
#
# It uses the `pep8` executable in `$PATH`. The configuration files `tox.ini`
# and `setup.cfg` are used if available, but are not required.

set -e

readonly path="$1"
shift

if ! [ -f "$path" ]; then
    echo >&2 'error: could not find file to format: '"$path"
    exit 4
fi

if ! which "pep8" > /dev/null; then
    echo >&2 'error: could not find the `pep8` binary'
    exit 3
fi

# Since this is just a checker, append the output to the file we are checking
# to cause it to have a diff and trip the check logic.
if ! pep8 "$path" >> "$path"; then
    readonly exit_code="$?"
    # The `pep8` tool exits with 1 if it detected problems; we append to a
    # file, so this already triggers the check as failing. Paper over this.
    if [ "$exit_code" = "1" ]; then
        exec true
    else
        exit "$exit_code"
    fi
fi
