#!/bin/sh
# This formatter uses the `black` tool to perform formatting checks of files
# written in Python.
#
# It uses the `black` executable in `$PATH`. The configuration file is named
# `pyproject.toml` in any parent directory of the target file. They are
# optional, but are used if they exist.

set -e

readonly path="$1"
shift

if [ "$#" -ne 0 ]; then
    version="-$1"
    arg="--required-version $1"
    shift
else
    version=""
    arg=""
fi
readonly version
readonly arg

if ! [ -f "$path" ]; then
    echo >&2 'error: could not find file to format: '"$path"
    exit 4
fi

if ! type "black$version" > /dev/null; then
    echo >&2 'error: could not find the `black` binary'
    exit 3
fi

exec "black$version" $arg "$path"
